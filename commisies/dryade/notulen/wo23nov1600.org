#+title:    Intro vergadering dryadecommisie
#+date:     Donderdag 23 november 16:00
#+author:   Philip Sokolov
#+email:    p.s.j.sokolov@students.uu.nl
#+language: nl
#+options:  toc:nil#+title:    Speed vergadering dryadecommisie
#+date:     Dinsdag 28 november 11:00
#+author:   Philip Sokolov
#+email:    p.s.j.sokolov@students.uu.nl
#+language: nl
#+options:  toc:nil
#+startup:  nofold

Er is besloten een ander cafe dan mick o' connels te zoeken.

#+startup:  nofold

* Primair
Philip Was een halfuur te laat (OV).
* Leden
- Jorit-Jan (JJ), Philip, Marieke.
* Rolverdeling Beschrijving
** Primaire Rollen
*** Voorzitter
Zit vergaderingen voor, maakt de agenda.
*** Secretaris
Houdt de notulen bij, regelt officeel contact met buitenwereld.
*** Penningmeester
Houdt de balans bij, heeft contact met de penningmeester van de FUF (Stef).
** Secundaire Rollen
*** Duurzaamheid
Kijkt naar de klimaat impact van deze commisie binnen de FUF.
Onderhoudt een lijntje met de duurzaamheidscommise / Gruncie.
*** Witboek
Maakt aan het einde van het jaar het witboek.

Het witboek is een document waarin staat wat je allemaal liever wel en niet wilt
gaan doen, een soort wiki ofzo? Meer een soort van protocol.
*** Foto
Maakt foto's tijdens activiteiten, geeft die aan bestuur. Gaat rond met de FUF
camera, die nog steeds vermist is...
* Rolverdeling Toeschrijving
| naam           | rol              |
|----------------+------------------|
| Voorzitter     | JJ               |
| Secretaris     | Philip           |
| Penningmeester | Marieke          |
| Duurzaamheid   | Philip           |
| Witboek        | Philip & Marieke |
| Foto           | Rouleren         |
* Data
| Data    | Event              |
|---------+-------------------- |
| 8 dec   | Docenten Cafe      |
| 7 feb   | Debat / Rollenspel |
| 8 mei   | Docenten Cafe      |
| Q3 2023 | Filosofie Festival |
** Andere mogelijkheden
*** Socratische gesprekken
Er is een oud FUF-lid die socratishe gesprekken voert, die wil misschien wel
iets doen.
*** Andere dingen
Je mag altijd zelf nog andee events doenmaar dit is een soort van de
planning.
* TODO Docenten Cafe 8 December [0/4] [0%]
Planning is een beetje krap, dat komt omdat deze commise laat is opgestart.

Bedoeloing is dat je een docent regelt, die iets gaat vertellen waar die mee
bezig is. Dit om de band tussen studenten en docenten te sterken.
1. [ ] Locatie vinden.
2. [ ] Docent vinden.
3. [ ] Cadeau regelen.

** Mogelijke locaties [1/6] [16%]
- [X] Mick O'Connels. Goede locatie volgens witboek, gratis te huren mits we
  ervoor zorgen dat men gaat drinken en ~200eur uitgeeft.
- [ ] Tussentijd. Filosofie cafe wordt hier ook gegeven, redelijke ruimte.
- [ ] Hofman
- [ ] De bastaard.
- [ ] De stadsgenoot.
- [ ] De Stichtse. Hier is de maandelijke borrel, waarschijnlijk geen goede
  optie, want: 1. Geen apparte ruimte. 2. Vrij ver weg.
