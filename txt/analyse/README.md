# Wat is dit?
Dit is de directory waar de bestanden voor de analyse staan.

index.ods -- de spreadsheet met alle titels.
README.md -- dit bestand.

# Opmerkingen
## Stijlen
Er is gekozen voor ruw weg drie soorten 'stijlen', omdat dat mijns inzien lijkt hoe 
de interesses in de contemporaine filosofie meestal worden ingedeeld.

Alles voor Kant is "Historisch". Na Kant breng ik de distincite  aan tussen
"Analytisch" en "Continentaal". Analytisch is de zogenaamde analytische filo.
Continentaal is alle Westerse filosofie, die niet Analytisch is (ruw maar het werkt).

Het neerzetten van de divergentie tussen analytische en "continentale" filo,
op dit punt in de tijd, zal voor sommigen veel te vroeg zijn, misschien zelfs anachronistisch.
Ze zouden het divergeerpunt waarschijnlijk ergens in de vroege twintigste eeuwe leggen.
Ten tijde van Heidegger en de Logisch positivisten zoals Carnap en hun geschillen.
Ik denk echter dat je deze indeling kunt maken, omdat de interesse (en citaties) van de meeste
auteurs die in deze stijlen worden ingedeeld merbaar begint te divergeren na Kant. 
(Niet per se chronologisch maar genealogisch -- dus onder invloed staand van).
Een analytische filosoof zal denk ik niet zo snel Hegel of Nietzsche uit de kast halen en een 
continentale filosoof zal het niet zo snel hebben over Mill of Frege. 

## Periodes
De indeling in periodes (van de westerse filo) doe ik als volgt.
Alles voor Socrates presocratisch (Plato inlcus).
Alles na Socrates is Heleens.

Alles na de val van het Romeinse Rijk is Scholastisch.
Alles na Descartes is Modern.
Bijna alles na de tweede oorlog is Universitair.

Met universitair bedoel ik dat het een format betreft dat enige "academische riguer" heeft.
Aka het kan geschreven zijn door iemand die werkt voor / aan een universiteit. 
En het papper houdt een format aan, dat zich probeert te spiegelen aan papers in
de (beta)wetenschappen. Andere indicatie kan zijn dat het gepubliceerd zou kunnen zijn 
in een Journal en onderhevig is aan eisen zoals peer review en fatsoenlijke bibliografie. 


Resultaten van wat basale analyse in R.
```
> read.csv('jaar1.csv') %>% group_by(Stijl) %>% summarise(percent=100*n() / nrow(data))
# A tibble: 4 × 2
  Stijl        percent
  <chr>          <dbl>
1 Analytisch     55.2 
2 Continentaal   16.7 
3 Historisch     27.1 
4 Japans          1.04
> read.csv('jaar1.csv') %>% group_by(Beschaving) %>% summarise(percent=100*n() / nrow(data))
# A tibble: 8 × 2
  Beschaving    percent
  <chr>           <dbl>
1 Angelsaksisch   51.0 
2 Christelijk      2.08
3 Deens            1.04
4 Duits           18.8 
5 Frans            8.33
6 Grieks          10.4 
7 Japans           1.04
8 Nederlands       7.29
> read.csv('jaar1.csv') %>% group_by(Nationaliteit) %>% summarise(percent=100*n() / nrow(data))
# A tibble: 27 × 2
   Nationaliteit       percent
   <chr>                 <dbl>
 1 Alexandria, Rome       1.04
 2 Amerikaans            27.1 
 3 Atheens                3.12
 4 Australisch            2.08
 5 Brits                 11.5 
 6 Brits & Australisch    1.04
 7 Brits, Schots          1.04
 8 Canadees               1.04
 9 Christenlijk           2.08
10 Deens                  1.04
# … with 17 more rows
# ℹ Use `print(n = ...)` to see more rows
> read.csv('jaar1.csv') %>% group_by(Taal) %>% summarise(percent=100*n() / nrow(data))
# A tibble: 8 × 2
  Taal       percent
  <chr>        <dbl>
1 Deens         1.04
2 Duits        17.7 
3 Engels       52.1 
4 Frans         7.29
5 Grieks       10.4 
6 Japans        1.04
7 Latijn        6.25
8 Nederlands    4.17
> read.csv('jaar1.csv') %>% group_by(Regio) %>% summarise(percent=100*n() / nrow(data))
# A tibble: 4 × 2
  Regio       percent
  <chr>         <dbl>
1 “Afrikaans”    1.04
2 Dekoloniaal    1.04
3 Japans         1.04
4 Westers       96.9 
> read.csv('jaar1.csv') %>% group_by(Deelgebied1) %>% summarise(percent=100*n() / nrow(data))
# A tibble: 13 × 2
   Deelgebied1          percent
   <chr>                  <dbl>
 1 epistemologie          22.9 
 2 ethiek                  2.08
 3 Ethiek                 18.8 
 4 Existentialisme         1.04
 5 Filo vd Geest           8.33
 6 Geschiedenis            3.12
 7 ideologie               1.04
 8 Logica                  3.12
 9 Meta-Filosofie          2.08
10 metaphysica             9.38
11 Politiek               24.0 
12 theologie               1.04
13 wetenschapsfilosofie    3.12
> 
```
