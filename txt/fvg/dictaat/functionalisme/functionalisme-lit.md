# Functionalisme Literatuur
Lycan & Prinz, eds., Mind and Cognition, Third edition.
Part I Ontology: The Identity Theory and Functionalism

4. Hilary Putnam, "The Nature of Mental States", pp. 40-47.
5. Ned Block, "Troubles with functionalism", pp. 48-52.
7. William G. Lycan, "The Contuinity of Levels of Nature", pp. 69-84.
Rey, Contemporary Philosophy of Mind
6. Functionalism: Commonalities
7. Functionalism: Differences

