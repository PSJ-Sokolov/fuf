# Larry Temkin – Obligations to the Needy
https://www.practicalethics.ox.ac.uk/uehiro-lectures-2017
Dit is een lezingenreeks van Larry Temkin die aansluit op de discussie die we naar aanleiding van Ord in de werkgroepen hebben gehad. Temkin gaat onder andere in op verschillende kanttekeningen die je kunt maken bij het idee dat geld geven aan goede doelen moreel juist of verplicht is. Voor wie het interessant vindt om zich hier wat meer in te verdiepen (dit is geen tentamenstof).

