# bot.py
import discord
from discord.ext import commands

TOKEN = input("PLEASE PROVIDE TOKEN: ")

intents = discord.Intents.default()
intents.message_content = True
bot = commands.Bot(command_prefix='>', intents=intents)

@bot.command()
async def ping(ctx):
    await ctx.send('pong')

bot.run(TOKEN)

# @client.event
# async def on_ready():
#     for guild in client.guilds:
#         print(f"GUILD: {guild.name}")
#         print(f"ID:    {guild.id}")
#         print()
# 
# client.run(TOKEN)
